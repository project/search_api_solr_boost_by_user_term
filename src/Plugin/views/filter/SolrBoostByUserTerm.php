<?php

namespace Drupal\search_api_solr_boost_by_user_term\Plugin\views\filter;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\filter\FilterPluginBase;

/**
 * Solr boost by date.
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("search_api_solr_boost_by_user_term")
 */
class SolrBoostByUserTerm extends FilterPluginBase {

  public function adminSummary() {}

  protected function operatorForm(&$form, FormStateInterface $form_state) {}

  public function canExpose() {
    return FALSE;
  }

  public function query() {
    $boost = Xss::filterAdmin($this->options['boost']);

    $content_field = Xss::filterAdmin($this->options['content_field']);
    $index = $this->query->getIndex();
    $fields = $index->getServerInstance()->getBackend()->getSolrFieldNames($index);
    $solr_field = !empty($fields[$content_field]) ? $fields[$content_field] : '';

    $user_proxy = \Drupal::currentUser();
    if (!$user_proxy->isAuthenticated() or empty($solr_field)) {
      // No need to process for anonymous users or if field can't be found.
      return;
    }
    $user_field = Xss::filterAdmin($this->options['user_field']);
    $user = \Drupal::entityTypeManager()->getStorage('user')->load($user_proxy->id());
    $user_val = $user->get($user_field)->target_id;
    if (empty($user_val)) {
      // No need to process if no user value set.
      return;
    }

    $boost_factors = $this->query->getOption('solr_document_boost_factors', []);
    $boost_factors[$content_field] = "if(exists(query(\$qq)),{$boost},1.0)";
    $this->query->setOption('solr_boost_more_recent', $boost_factors);
    $this->query->setOption('solr_document_boost_factors', $boost_factors);
    $this->query->setOption('solr_param_qq', "{$solr_field}:{$user_val}");

  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    $user_field = Xss::filterAdmin($this->options['user_field']);
    return Cache::mergeContexts(parent::getCacheContexts(), ['user.ref_field:' . $user_field]);
  }

  /**
   * {@inheritdoc}
   */
  public function defineOptions() {
    $options = parent::defineOptions();

    $options['user_field']['default'] = '';
    $options['content_field']['default'] = '';
    $options['boost']['default'] = '1';
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $field_storage = \Drupal::entityTypeManager()->getStorage('field_storage_config');
    $user_fields_raw = $field_storage->loadByProperties(
      [
        'settings' => [
          'target_type' => 'taxonomy_term',
        ],
        'entity_type' => 'user',
        'type' => 'entity_reference',
        'deleted' => FALSE,
        'status' => 1,
      ]
    );
    $field_config = \Drupal::entityTypeManager()->getStorage('field_config');
    // Load user field labels.
    $user_fields_config = $field_config->loadByProperties(
      [
        'entity_type' => 'user',
        'field_type' => 'entity_reference',
        'status' => 1,
      ]
    );
    $field_labels = [];
    foreach ($user_fields_config as $user_field) {
      $field_labels[$user_field->getName()] = $user_field->getLabel();
    }
    foreach ($user_fields_raw as $user_field_raw) {
      $user_fields[$user_field_raw->getName()] = $field_labels[$user_field_raw->getName()] ?? $user_field_raw->getName();
    }
    // @todo make compatible with more than nodes?
    $node_fields_raw = $field_storage->loadByProperties(
      [
        'settings' => [
          'target_type' => 'taxonomy_term',
        ],
        'entity_type' => 'node',
        'type' => 'entity_reference',
        'deleted' => FALSE,
        'status' => 1,
      ]
    );
    $node_fields_config = $field_config->loadByProperties(
      [
        'entity_type' => 'node',
        'field_type' => 'entity_reference',
        'status' => 1,
      ]
    );
    $field_labels = [];
    foreach ($node_fields_config as $node_field) {
      $field_labels[$node_field->getName()] = $node_field->getLabel();
    }
    foreach ($node_fields_raw as $node_field_raw) {
      $node_fields[$node_field_raw->getName()] = $field_labels[$node_field_raw->getName()] ?? $node_field_raw->getName();
    }

    $form['user_field'] = [
      '#type' => 'select',
      '#options' => $user_fields,
      '#title' => $this->t('User field'),
      '#default_value' => $this->options['user_field'],
      '#description' => $this->t('The user profile field from which to retrieve the term.'),
      '#required' => TRUE,
    ];

    $form['content_field'] = [
      '#type' => 'select',
      '#options' => $node_fields,
      '#title' => $this->t('Content field'),
      '#default_value' => $this->options['content_field'],
      '#description' => $this->t('The content field in which boost a match.'),
      '#required' => TRUE,
    ];

    $form['boost'] = [
      '#type' => 'select',
      '#default_value' => $this->options['boost'],
      '#title' => $this->t('Boost value'),
      '#options' => [
        '0.0' => '0.0',
        '0.1' => '0.1',
        '0.2' => '0.2',
        '0.3' => '0.3',
        '0.5' => '0.5',
        '0.8' => '0.8',
        '1.0' => '1.0',
        '2.0' => '2.0',
        '3.0' => '3.0',
        '5.0' => '5.0',
        '8.0' => '8.0',
        '13.0' => '13.0',
        '21.0' => '21.0',
      ],
      '#description' => $this->t('The amount by which to boost results with a matching term value.'),
      '#required' => TRUE,
    ];

  }

}
