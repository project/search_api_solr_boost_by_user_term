CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module allows a search view to add a boost query on a field value that
matches a field in the current user's profile. For example, if your site had a
taxonomy for animals, and a user had set their favourite animal as "elephant"
in their user profile, the module would cause content tagged with "elephant"
to appear higher in the search results.

The boosts are applied at query time, so configuration changes can be made
without requiring a reindex. The module will also force the query processor to
be EDismax.

Note that in its current version the module adds a "user" cache context, which
means that the cache will store different search results per user, and
invalidate the user's cache if they update their profile.


INSTALLATION
------------

 * Install this module as you would normally install a contributed Drupal
   module. Visit https://www.drupal.org/node/1897420 for further information.


REQUIREMENTS
------------

This module requires the Search API Solr module, and a search view that shows
results from a Solr-based index.


CONFIGURATION
-------------

 * Add the Boost By User Term filer, and select the node and user fields to use.
 * Specify the amount of boost to add for matches. A lower result will make for
   a more subtle alteration of the search results, and a highger value will
   create a more obvious effect.

MAINTAINERS
-----------

 * Current Maintainer: Martin Anderson-Clutz (mandclu) - https://www.drupal.org/u/mandclu
